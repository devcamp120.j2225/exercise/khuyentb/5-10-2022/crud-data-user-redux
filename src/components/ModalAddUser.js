import { Alert, Box, Button, fabClasses, FormControl, Input, MenuItem, Modal, Select, Snackbar, Typography } from "@mui/material"
import { Col, Row } from "reactstrap"
import { useEffect, useState } from "react";


function ModalAddUser({ style, openProps, closeProps, refreshPage, setRefreshPage, getData }) {

    const [firstNameForm, setFirstName] = useState("");
    const [lastNameForm, setLastName] = useState("");
    const [countryForm, setCountry] = useState("VN");
    const [registerStatusForm, setRegisterStatus] = useState("Accepted");
    const [subjectForm, setSubject] = useState("");

    const [statusModal, setStatusModal] = useState("error");
    const [openAlert, setOpenAlert] = useState(false);
    const [alertContent, setAlertContent] = useState("")


    const onFirstnameChange = (event) => {
        setFirstName(event.target.value)
    }

    const onLastnameChange = (event) => {
        setLastName(event.target.value)
    }

    const onSelectCountryChange = (event) => {
        setCountry(event.target.value)
    }

    const onSelectRegisterStatusChange = (event) => {
        setRegisterStatus(event.target.value)
    }

    const onSubjectChange = (event) => {
        setSubject(event.target.value)
    }

    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    const onBtnInsertClick = () => {
        console.log("Insert được click!")
        var dataAdd = {
            firstName: firstNameForm,
            lastName: lastNameForm,
            country: countryForm,
            registerStatus: registerStatusForm,
            subject: subjectForm
        }

        var checkData = (validateData(dataAdd));
        console.log(dataAdd)
        if (checkData) {
            const body = {
                method: "POST",
                body: JSON.stringify({
                    firstname: dataAdd.firstName,
                    lastname: dataAdd.lastName,
                    country: dataAdd.country,
                    registerStatus: dataAdd.registerStatus,
                    subject: dataAdd.subject
                }),
                header: {
                    'Content-type': 'application/json; charset=UTF-8',
                }
            }

            getData('http://203.171.20.210:8080/crud-api/users/', body )
            .then((data) => {
                console.log(data);
                setOpenAlert(true);
                setStatusModal("success")
                setAlertContent("Add user successfully!");
                setRefreshPage(refreshPage + 1)
                closeProps();
            })
            .catch((err) => {
                console.log(err.message);
                setOpenAlert(true);
                setStatusModal("error")
                setAlertContent("Add user fail!");
                closeProps()
            })
        }
    }

    const validateData = (paramDataAdd) => {
        if (!paramDataAdd.firstName) {
            setOpenAlert(true);
            setStatusModal("error");
            setAlertContent("FirstName is invalid!")
            return false
        }
        if (!paramDataAdd.lastName) {
            setOpenAlert(true);
            setStatusModal("error");
            setAlertContent("LastName is invalid!")
            return false
        }
        if (!paramDataAdd.subject) {
            setOpenAlert(true);
            setStatusModal("error");
            setAlertContent("Subject is invalid!")
            return false
        }
        else {
            return true
        }
       
    }

    const onBtnCancelClick = () => {
        closeProps()
    }



    return (
        <>
            <Modal
                open={openProps}
                onClose={closeProps}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                        <strong>Thêm User!</strong>
                    </Typography>
                    <Row>
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Firstname:</label>
                                </Col>
                                <Col sm="7">
                                    <Input onChange={onFirstnameChange} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Lastname:</label>
                                </Col>
                                <Col sm="7">
                                    <Input onChange={onLastnameChange} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Country:</label>
                                </Col>
                                <Col sm="7">
                                    <Select
                                        id="country-select"
                                        defaultValue="VN"
                                        fullWidth
                                        onChange={onSelectCountryChange}
                                    >
                                        <MenuItem value="VN">Việt Nam</MenuItem>
                                        <MenuItem value="USA">USA</MenuItem>
                                        <MenuItem value="AUS">Australia</MenuItem>
                                        <MenuItem value="CAN">Canada</MenuItem>
                                    </Select>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Register Status:</label>
                                </Col>
                                <Col sm="7">
                                    <Select
                                        id="registerstatus-select"
                                        defaultValue="Accepted"
                                        fullWidth
                                        onChange={onSelectRegisterStatusChange}
                                    >
                                        <MenuItem value="Accepted">Accepted</MenuItem>
                                        <MenuItem value="Denied">Denied</MenuItem>
                                        <MenuItem value="Standard">Standard</MenuItem>
                                    </Select>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Subject:</label>
                                </Col>
                                <Col sm="7">
                                    <Input onChange={onSubjectChange} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-4 text-center">
                        <Col sm="12">
                            <Row className="mt-4">
                                <Col sm="6">
                                    <Button onClick={onBtnInsertClick} className="bg-success w-75 text-white">Insert User</Button>
                                </Col>
                                <Col sm="6">
                                    <Button onClick={onBtnCancelClick} className="bg-success w-75 text-white">Hủy Bỏ</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {alertContent}
                </Alert>
            </Snackbar>
        </>

    )
}

export default ModalAddUser