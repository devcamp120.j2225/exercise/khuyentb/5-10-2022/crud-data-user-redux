import { Alert, Box, Button, Input, MenuItem, Modal, Select, Snackbar, Typography } from "@mui/material"
import { Col, Row } from "reactstrap"
import { useEffect, useState } from "react";


function ModalEditUser({ style, openProps, closeProps, refreshPage, setRefreshPage, getData, rowClicked }) {

    const [idEdit, setIdEdit] = useState("")
    const [firstnameEditForm, setFirstNameForm] = useState("");
    const [lastnameEditForm, setLastNameForm] = useState("");
    const [countryEditForm, setCountryForm] = useState("");
    const [customerTypeEditForm, setCustomerTypeEditForm] = useState("");
    const [registerStatusEditForm, setRegisterStatusEditForm] = useState("");
    const [subjectEditForm, setSubjectEditForm] = useState("")


    const [openAlert, setOpenAlert] = useState(false);
    const [statusModal, setStatusModal] = useState("error");
    const [alertContent, setAlertContent] = useState("")


    const onFirstnameChange = (event) => {
        setFirstNameForm(event.target.value)
    }

    const onLastnameChange = (event) => {
        setLastNameForm(event.target.value)
    }

    const onSelectCountryChange = (event) => {
        setCountryForm(event.target.value)
    }

    const onSelectCustomerTypeChange = (event) => {
        setCustomerTypeEditForm(event.target.value)
    }

    const onSelectRegisterStatusChange = (event) => {
        setRegisterStatusEditForm(event.target.value)
    }

    const onSubjectChange = (event) => {
        setSubjectEditForm(event.target.value)
    }

    const onBtnUpdateClick = () => {

        console.log("Edit Button Clicked!!!")

        var checkData = (validateData());
        if (checkData) {
            const body = {
                method: "PUT",
                body: JSON.stringify({
                    firstname: firstnameEditForm,
                    lastname: lastnameEditForm,
                    country: countryEditForm,
                    customerType: customerTypeEditForm,
                    registerStatus: registerStatusEditForm,
                    subject: subjectEditForm
                }),
                header: {
                    'Content-type': 'application/json; charset=UTF-8',
                }
            }
            

            getData('http://203.171.20.210:8080/crud-api/users/' + idEdit, body)
                .then((data) => {
                    console.log(data);
                    setOpenAlert(true);
                    setAlertContent("Edit sucessfully");
                    setStatusModal("success");
                    setRefreshPage(refreshPage + 1);
                    closeProps();
                })
                .catch((error) => {
                    console.log(error.message);
                    setOpenAlert(false);
                    setAlertContent("Edit fail");
                    setStatusModal("error");
                    closeProps();
                })
        }

    }

    const validateData = () => {
        if (firstnameEditForm === "") {
            setOpenAlert(true);
            setAlertContent("Firstname chưa được điền!")
            setStatusModal("error")
            return false
        }
        if (lastnameEditForm === "") {
            setOpenAlert(true);
            setAlertContent("Lastname chưa được điền!")
            setStatusModal("error")
            return false
        }
        if (!(countryEditForm === "VN" || countryEditForm === "USA" || countryEditForm === "AUS" || countryEditForm === "CAN") || countryEditForm === null) {
            setOpenAlert(true);
            setAlertContent("Country Cần chọn lại cho đúng!")
            setStatusModal("error")
            return false
        }
        if (!(customerTypeEditForm === "Standard" || customerTypeEditForm === "Gold" || customerTypeEditForm === "Premium") || customerTypeEditForm === null) {
            setOpenAlert(true);
            setAlertContent("Customer Type cần chọn lại cho đúng!")
            setStatusModal("error")
            return false
        }
        if (!(registerStatusEditForm === "Standard" || registerStatusEditForm === "Accepted" || registerStatusEditForm === "Denied") || customerTypeEditForm === null) {
            setOpenAlert(true);
            setAlertContent("Register Status cần chọn lại cho đúng!")
            setStatusModal("error")
            return false
        }
        if (subjectEditForm === "") {
            setOpenAlert(true);
            setAlertContent("Subject chưa được điền!")
            setStatusModal("error")
            return false
        }
        else {
            return true
        }

    }

    const onBtnCancelClick = () => {
        closeProps()
    }

    const handleCloseAlert = () => {
        setOpenAlert(false)
    }


    useEffect(() => {
        console.log(rowClicked.id)
        setIdEdit(rowClicked.id);
        setFirstNameForm(rowClicked.firstname);
        setLastNameForm(rowClicked.lastname);
        setCountryForm(rowClicked.country);
        setCustomerTypeEditForm(rowClicked.customerType);
        setRegisterStatusEditForm(rowClicked.registerStatus);
        setSubjectEditForm(rowClicked.subject)
    }, [openProps])

    return (
        <>
            <Modal
                open={openProps}
                onClose={closeProps}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                        <strong>Edit User!</strong>
                    </Typography>
                    <Row>
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>ID:</label>
                                </Col>
                                <Col sm="7">
                                    <Input readOnly value={idEdit} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Firstname:</label>
                                </Col>
                                <Col sm="7">
                                    <Input onChange={onFirstnameChange} value={firstnameEditForm} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>

                    <Row className="mt-2">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Lastname:</label>
                                </Col>
                                <Col sm="7">
                                    <Input onChange={onLastnameChange} value={lastnameEditForm} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Country:</label>
                                </Col>
                                <Col sm="7">
                                    <Select
                                        id="country-select"
                                        value={countryEditForm ? countryEditForm : ""}
                                        defaultValue={countryEditForm ? countryEditForm : ""}
                                        fullWidth
                                        onChange={onSelectCountryChange}
                                    >
                                        <MenuItem value="VN">Việt Nam</MenuItem>
                                        <MenuItem value="USA">USA</MenuItem>
                                        <MenuItem value="AUS">Australia</MenuItem>
                                        <MenuItem value="CAN">Canada</MenuItem>
                                    </Select>

                                    {!(countryEditForm === "VN" || countryEditForm === "USA" || countryEditForm === "AUS" || countryEditForm === "CAN") ? <i style={{ fontSize: "10px", color: "red" }}>Country không nằm trong select dữ liệu!</i> : null}

                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Customer Type:</label>
                                </Col>
                                <Col sm="7">
                                    <Select
                                        id="customertype-select"
                                        value={customerTypeEditForm ? customerTypeEditForm : ""}
                                        defaultValue={customerTypeEditForm ? customerTypeEditForm : ""}
                                        fullWidth
                                        onChange={onSelectCustomerTypeChange}
                                    >
                                        <MenuItem value="Standard">Standard</MenuItem>
                                        <MenuItem value="Gold">Gold</MenuItem>
                                        <MenuItem value="Premium">Premium</MenuItem>
                                    </Select>
                                    {!(customerTypeEditForm === "Standard" || customerTypeEditForm === "Gold" || customerTypeEditForm === "Premium") ? <i style={{ fontSize: "10px", color: "red" }}>Customer Type không nằm trong select dữ liệu!</i> : null}
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Register Status:</label>
                                </Col>
                                <Col sm="7">
                                    <Select
                                        id="registerstatus-select"
                                        value={registerStatusEditForm ? registerStatusEditForm : ""}
                                        defaultValue={registerStatusEditForm ? registerStatusEditForm : ""}
                                        fullWidth
                                        onChange={onSelectRegisterStatusChange}
                                    >
                                        <MenuItem value="Accepted">Accepted</MenuItem>
                                        <MenuItem value="Denied">Denied</MenuItem>
                                        <MenuItem value="Standard">Standard</MenuItem>
                                    </Select>
                                    {!(registerStatusEditForm === "Accepted" || registerStatusEditForm === "Denied" || registerStatusEditForm === "Standard") ? <i style={{ fontSize: "10px", color: "red" }}>Register Status không nằm trong select dữ liệu!</i> : null}
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="12">
                            <Row>
                                <Col sm="5">
                                    <label>Subject:</label>
                                </Col>
                                <Col sm="7">
                                    <Input onChange={onSubjectChange} value={subjectEditForm} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-4 text-center">
                        <Col sm="12">
                            <Row className="mt-4">
                                <Col sm="6">
                                    <Button onClick={onBtnUpdateClick} className="bg-success w-100 text-white">Update User</Button>
                                </Col>
                                <Col sm="6">
                                    <Button onClick={onBtnCancelClick} className="bg-success w-75 text-white">Hủy Bỏ</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {alertContent}
                </Alert>
            </Snackbar>
        </>
    )
}

export default ModalEditUser;