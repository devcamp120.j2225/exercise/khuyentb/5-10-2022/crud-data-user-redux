import { Button, Typography, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Paper, Grid, Pagination } from "@mui/material";
import { Container } from "@mui/system";
import { useEffect, useState } from "react";
import ModalAddUser from "./ModalAddUser";
import ModalDeleteUser from "./ModalDelete";
import ModalEditUser from "./ModalEdit";

function DataUser() {

    const limitRow = 10;
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    const [rows, setRows] = useState([]);
    const [currentPage, setCurrentPage] = useState(1)
    const [totalPage, setTotalPage] = useState(0)
    const [refreshPage, setRefreshPage] = useState(0)
    const [rowClicked, setRowClicked] = useState([])


    const [openModalAdd, setOpenModalAdd] = useState(false);
    const [openModalEdit, setOpenModalEdit] = useState(false);
    const [openModalDelete, setOpenModalDelete] = useState(false);


    const closeModalAdd = () => setOpenModalAdd(false);
    const closeModalEdit = () => setOpenModalEdit(false);
    const closeModalDelete = () => setOpenModalDelete(false);


    const onButtonAddClick = () => {
        setOpenModalAdd(true)
    }

    const onButtonEditClick = (row) => {
        setOpenModalEdit(true)
        setRowClicked(row)
        console.log(row)
    }

    const onButtonDeleteClick = (row) => {
        setOpenModalDelete(true)
        setRowClicked(row)
        console.log(row)
    }

    const getData = async (paramUrl, paramOption = {}) => {
        const response = await fetch(paramUrl, paramOption);
        const data = await response.json();
        return data
    }

    const changePageHandler = (event, value) => {
        console.log(event);
        console.log(value)
        setCurrentPage(value);
    }


    useEffect(() => {
        getData("http://203.171.20.210:8080/crud-api/users/")
            .then((data) => {
                console.log(data);
                setRows(data.slice((currentPage - 1) * limitRow, currentPage * limitRow));
                setTotalPage(Math.ceil(data.length/limitRow))
            })
            .catch((err) => {
                console.log(err.message)
            })
    }, [currentPage, refreshPage])

    
    return (
        <Container>
            <Grid>
                <Typography style={{ fontWeight: "bold", fontSize: "50px" }} mt={5} className="text-center">DANH SÁCH NGƯỜI DÙNG ĐĂNG KÝ</Typography>
            </Grid>
            <Grid>
                <Button variant='contained' color="info" className="mt-5" onClick={onButtonAddClick}>Thêm Người Dùng</Button>
            </Grid>
            <Grid>
                <TableContainer component={Paper} className="mt-2">
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="center"><b>ID</b></TableCell>
                                <TableCell align="center"><b>FirstName</b></TableCell>
                                <TableCell align="center"><b>LastName</b></TableCell>
                                <TableCell align="center"><b>Country</b></TableCell>
                                <TableCell align="center"><b>Subject</b></TableCell>
                                <TableCell align="center"><b>Customer Type</b></TableCell>
                                <TableCell align="center"><b>Register Status</b></TableCell>
                                <TableCell align="center"><b>Action</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map((row) => (
                                <TableRow
                                    key={row.id}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row">
                                        {row.id}
                                    </TableCell>
                                    <TableCell align="center">{row.firstname}</TableCell>
                                    <TableCell align="center">{row.lastname}</TableCell>
                                    <TableCell align="center">{row.country}</TableCell>
                                    <TableCell align="center">{row.subject}</TableCell>
                                    <TableCell align="center">{row.customerType}</TableCell>
                                    <TableCell align="center">{row.registerStatus}</TableCell>
                                    <TableCell>
                                        <Button variant='contained' color="success" style={{ marginRight: "10px" }} onClick={() => {onButtonEditClick(row)}}>Sửa</Button>
                                        <Button variant='contained' color="error" onClick={() => {onButtonDeleteClick(row)}}>Xóa</Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
            <Grid container justifyContent="flex-end" my={4}>
                <Grid item>
                    <Pagination count={totalPage} defaultPage={currentPage} onChange={changePageHandler} variant="outlined" color="primary"></Pagination>
                </Grid>
            </Grid>

            {/* Modal Add User */}
            <ModalAddUser
            getData={getData}
            style={style}
            openProps={openModalAdd}
            closeProps={closeModalAdd}
            refreshPage={refreshPage}
            setRefreshPage={setRefreshPage}>
            </ModalAddUser>

            {/* Modal Edit User */}
            <ModalEditUser
            getData={getData}
            style={style}
            openProps={openModalEdit}
            closeProps={closeModalEdit}
            refreshPage={refreshPage}
            setRefreshPage={setRefreshPage}
            rowClicked={rowClicked}>
            </ModalEditUser>

            <ModalDeleteUser
            openModalDelete={openModalDelete}
            idDelete={rowClicked.id}
            handleCloseDelete={closeModalDelete}
            style={style}
            setVarRefeshPage={setRefreshPage}
            varRefeshPage={refreshPage}>
            </ModalDeleteUser>
        </Container>
    )
}

export default DataUser;