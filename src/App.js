import './App.css';
import DataUser from './components/DataUser';
import "bootstrap/dist/css/bootstrap.min.css"

function App() {
  return (
    <div className="App">
      <DataUser></DataUser>
    </div>
  );
}

export default App;
